<?php

namespace MD\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CustomField
 *
 * @ORM\Table(name="custom_field")
 * @ORM\Entity(repositoryClass="MD\CRMBundle\Repository\CustomFieldRepository")
 */
class CustomField
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_custom_field", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="moral", type="text")
     */
    private $moral;

    /**
     * @ORM\ManyToOne(targetEntity="MD\UserBundle\Teacher")
     * @ORM\JoinColumn(name="id_teacher", referencedColumnName="id_teacher")
     */
    private $teacher;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return CustomField
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set moral
     *
     * @param string $moral
     *
     * @return CustomField
     */
    public function setMoral($moral)
    {
        $this->moral = $moral;

        return $this;
    }

    /**
     * Get moral
     *
     * @return string
     */
    public function getMoral()
    {
        return $this->moral;
    }

    /**
     * Set teacher
     *
     * @param integer $teacher
     *
     * @return CustomField
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return int
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return CustomField
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

