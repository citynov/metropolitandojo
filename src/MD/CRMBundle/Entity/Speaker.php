<?php

namespace MD\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Speaker
 *
 * @ORM\Table(name="speaker")
 * @ORM\Entity(repositoryClass="MD\CRMBundle\Repository\SpeakerRepository")
 */
class Speaker
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=20)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     * @var int
     *
     * @ORM\Column(name="id_activity", type="integer")
     */
    private $idActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1, nullable=true)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="text", nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="string", length=20, nullable=true)
     */
    private $experience;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="motivation", type="text", nullable=true)
     */
    private $motivation;

    /**
     * @var int
     *
     * @ORM\Column(name="id_address", type="integer", nullable=true)
     */
    private $idAddress;

    /**
     * @var float
     *
     * @ORM\Column(name="payment", type="float", nullable=true)
     */
    private $payment;

    /**
     * @var int
     *
     * @ORM\Column(name="id_group_adherent", type="integer", nullable=true)
     */
    private $idGroupAdherent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_lead", type="integer", nullable=true)
     */
    private $idLead;

    /**
     * @var int
     *
     * @ORM\Column(name="id_teacher", type="integer", nullable=true)
     */
    private $idTeacher;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Speaker
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Speaker
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Speaker
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Speaker
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set idActivity
     *
     * @param integer $idActivity
     *
     * @return Speaker
     */
    public function setIdActivity($idActivity)
    {
        $this->idActivity = $idActivity;

        return $this;
    }

    /**
     * Get idActivity
     *
     * @return int
     */
    public function getIdActivity()
    {
        return $this->idActivity;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Speaker
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Speaker
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set experience
     *
     * @param string $experience
     *
     * @return Speaker
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return string
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Speaker
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set motivation
     *
     * @param string $motivation
     *
     * @return Speaker
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;

        return $this;
    }

    /**
     * Get motivation
     *
     * @return string
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * Set idAddress
     *
     * @param integer $idAddress
     *
     * @return Speaker
     */
    public function setIdAddress($idAddress)
    {
        $this->idAddress = $idAddress;

        return $this;
    }

    /**
     * Get idAddress
     *
     * @return int
     */
    public function getIdAddress()
    {
        return $this->idAddress;
    }

    /**
     * Set payment
     *
     * @param float $payment
     *
     * @return Speaker
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return float
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set idGroupAdherent
     *
     * @param integer $idGroupAdherent
     *
     * @return Speaker
     */
    public function setIdGroupAdherent($idGroupAdherent)
    {
        $this->idGroupAdherent = $idGroupAdherent;

        return $this;
    }

    /**
     * Get idGroupAdherent
     *
     * @return int
     */
    public function getIdGroupAdherent()
    {
        return $this->idGroupAdherent;
    }

    /**
     * Set idLead
     *
     * @param integer $idLead
     *
     * @return Speaker
     */
    public function setIdLead($idLead)
    {
        $this->idLead = $idLead;

        return $this;
    }

    /**
     * Get idLead
     *
     * @return int
     */
    public function getIdLead()
    {
        return $this->idLead;
    }

    /**
     * Set idTeacher
     *
     * @param integer $idTeacher
     *
     * @return Speaker
     */
    public function setIdTeacher($idTeacher)
    {
        $this->idTeacher = $idTeacher;

        return $this;
    }

    /**
     * Get idTeacher
     *
     * @return int
     */
    public function getIdTeacher()
    {
        return $this->idTeacher;
    }
}

