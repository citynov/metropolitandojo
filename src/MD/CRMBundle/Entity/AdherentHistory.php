<?php

namespace MD\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdherentHistory
 *
 * @ORM\Table(name="adherent_history")
 * @ORM\Entity(repositoryClass="MD\CRMBundle\Repository\AdherentHistoryRepository")
 */
class AdherentHistory {

    /**
     * @var int
     *
     * @ORM\Column(name="id_adherent_history", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="year_begin", type="integer")
     */
    private $yearBegin;

    /**
     * @var int
     *
     * @ORM\Column(name="year_end", type="integer")
     */
    private $yearEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MainBundle\Adherent")
     * @ORM\JoinColumn(name="id_adherent", referencedColumnName="id_adherent")
     */
    private $adherent;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set yearBegin
     *
     * @param integer $yearBegin
     *
     * @return AdherentHistory
     */
    public function setYearBegin($yearBegin) {
        $this->yearBegin = $yearBegin;

        return $this;
    }

    /**
     * Get yearBegin
     *
     * @return int
     */
    public function getYearBegin() {
        return $this->yearBegin;
    }

    /**
     * Set yearEnd
     *
     * @param integer $yearEnd
     *
     * @return AdherentHistory
     */
    public function setYearEnd($yearEnd) {
        $this->yearEnd = $yearEnd;

        return $this;
    }

    /**
     * Get yearEnd
     *
     * @return int
     */
    public function getYearEnd() {
        return $this->yearEnd;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return AdherentHistory
     */
    public function setDateAdd($dateAdd) {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd() {
        return $this->dateAdd;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return AdherentHistory
     */
    public function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate() {
        return $this->dateUpdate;
    }

    /**
     * Set adherent
     *
     * @param integer $adherent
     *
     * @return AdherentHistory
     */
    public function setAdherent($adherent) {
        $this->adherent = $adherent;

        return $this;
    }

    /**
     * Get adherent
     *
     * @return int
     */
    public function getAdherent() {
        return $this->adherent;
    }

}
