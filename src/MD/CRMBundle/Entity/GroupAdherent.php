<?php

namespace MD\CRMBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupAdherent
 *
 * @ORM\Table(name="group_adherent")
 * @ORM\Entity(repositoryClass="MD\CRMBundle\Repository\GroupAdherentRepository")
 */
class GroupAdherent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_group_adherent", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MD\UserBundle\Teacher")
     * @ORM\JoinColumn(name="id_teacher", referencedColumnName="id_teacher")
     */
    private $teacher;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=30)
     */
    private $label;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teacher
     *
     * @param integer $teacher
     *
     * @return GroupAdherent
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return int
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return GroupAdherent
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
}

