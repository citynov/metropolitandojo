<?php

namespace MD\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MD\MainBundle\Entity\Page;

/**
 * UserPermission
 *
 * @ORM\Table(name="user_permission")
 * @ORM\Entity(repositoryClass="MD\UserBundle\Repository\UserPermissionRepository")
 */
class UserPermission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_user_permission", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="id_user", referencedColumnName="id_user")
     */
    private $user;

    /**
     * @var Page
     *
     * @ManyToOne(targetEntity="Page")
     * @JoinColumn(name="id_page", referencedColumnName="id_page")
     */
    private $page;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @var bool
     *
     * @ORM\Column(name="creatable", type="boolean")
     */
    private $creatable;

    /**
     * @var bool
     *
     * @ORM\Column(name="editable", type="boolean")
     */
    private $editable;

    /**
     * @var bool
     *
     * @ORM\Column(name="deletable", type="boolean")
     */
    private $deletable;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserPermission
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set page
     *
     * @param Page $page
     *
     * @return UserPermission
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return UserPermission
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return bool
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set creatable
     *
     * @param boolean $creatable
     *
     * @return UserPermission
     */
    public function setCreatable($creatable)
    {
        $this->creatable = $creatable;

        return $this;
    }

    /**
     * Get creatable
     *
     * @return bool
     */
    public function getCreatable()
    {
        return $this->creatable;
    }

    /**
     * Set editable
     *
     * @param boolean $editable
     *
     * @return UserPermission
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Get editable
     *
     * @return bool
     */
    public function getEditable()
    {
        return $this->editable;
    }

    /**
     * Set deletable
     *
     * @param boolean $deletable
     *
     * @return UserPermission
     */
    public function setDeletable($deletable)
    {
        $this->deletable = $deletable;

        return $this;
    }

    /**
     * Get deletable
     *
     * @return bool
     */
    public function getDeletable()
    {
        return $this->deletable;
    }

}

