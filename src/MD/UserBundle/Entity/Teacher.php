<?php

namespace MD\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MD\MainBundle\Entity\Image;

/**
 * Teacher
 *
 * @ORM\Table(name="teacher")
 * @ORM\Entity(repositoryClass="MD\UserBundle\Repository\TeacherRepository")
 */
class Teacher
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_teacher", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="id_user", referencedColumnName="id_user")
     */
    private $user;

    /**
     * @var Address
     *
     * @OneToOne(targetEntity="MD\UserBundle\Entity\Address")
     * @JoinColumn(name="id_address", referencedColumnName="id_address")
     */
    private $address;

    /**
     * @var Image
     *
     * @ManyToOne(targetEntity="Image")
     * @JoinColumn(name="id_image", referencedColumnName="id_image")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="video_link", type="string", length=100, nullable=true)
     */
    private $videoLink;

    /**
     * @var string
     *
     * @ORM\Column(name="social_link", type="string", length=100, nullable=true)
     */
    private $socialLink;

    /**
     * @var string
     *
     * @ORM\Column(name="licence", type="text", nullable=true)
     */
    private $licence;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=100, nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="proverbs", type="string", length=255, nullable=true)
     */
    private $proverbs;

    /**
     * @var string
     *
     * @ORM\Column(name="moral", type="text", nullable=true)
     */
    private $moral;

    /**
     * @var string
     *
     * @ORM\Column(name="corporate_name", type="string", length=50, nullable=true)
     */
    private $corporateName;

    /**
     * @var string
     *
     * @ORM\Column(name="prefecture", type="string", length=20, nullable=true)
     */
    private $prefecture;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=14, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="sponsorship_code", type="string", length=5, nullable=true)
     */
    private $sponsorshipCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Teacher
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Teacher
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set videoLink
     *
     * @param string $videoLink
     *
     * @return Teacher
     */
    public function setVideoLink($videoLink)
    {
        $this->videoLink = $videoLink;

        return $this;
    }

    /**
     * Get videoLink
     *
     * @return string
     */
    public function getVideoLink()
    {
        return $this->videoLink;
    }

    /**
     * Set image
     *
     * @param Image $image
     *
     * @return Teacher
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set socialLink
     *
     * @param string $socialLink
     *
     * @return Teacher
     */
    public function setSocialLink($socialLink)
    {
        $this->socialLink = $socialLink;

        return $this;
    }

    /**
     * Get socialLink
     *
     * @return string
     */
    public function getSocialLink()
    {
        return $this->socialLink;
    }

    /**
     * Set licence
     *
     * @param string $licence
     *
     * @return Teacher
     */
    public function setLicence($licence)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return string
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Teacher
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set proverbs
     *
     * @param string $proverbs
     *
     * @return Teacher
     */
    public function setProverbs($proverbs)
    {
        $this->proverbs = $proverbs;

        return $this;
    }

    /**
     * Get proverbs
     *
     * @return string
     */
    public function getProverbs()
    {
        return $this->proverbs;
    }

    /**
     * Set moral
     *
     * @param string $moral
     *
     * @return Teacher
     */
    public function setMoral($moral)
    {
        $this->moral = $moral;

        return $this;
    }

    /**
     * Get moral
     *
     * @return string
     */
    public function getMoral()
    {
        return $this->moral;
    }

    /**
     * Set corporateName
     *
     * @param string $corporateName
     *
     * @return Teacher
     */
    public function setCorporateName($corporateName)
    {
        $this->corporateName = $corporateName;

        return $this;
    }

    /**
     * Get corporateName
     *
     * @return string
     */
    public function getCorporateName()
    {
        return $this->corporateName;
    }

    /**
     * Set address
     *
     * @param Address $address
     *
     * @return Teacher
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set prefecture
     *
     * @param string $prefecture
     *
     * @return Teacher
     */
    public function setPrefecture($prefecture)
    {
        $this->prefecture = $prefecture;

        return $this;
    }

    /**
     * Get prefecture
     *
     * @return string
     */
    public function getPrefecture()
    {
        return $this->prefecture;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return Teacher
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set sponsorshipCode
     *
     * @param string $sponsorshipCode
     *
     * @return Teacher
     */
    public function setSponsorshipCode($sponsorshipCode)
    {
        $this->sponsorshipCode = $sponsorshipCode;

        return $this;
    }

    /**
     * Get sponsorshipCode
     *
     * @return string
     */
    public function getSponsorshipCode()
    {
        return $this->sponsorshipCode;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return Teacher
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Teacher
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
}

