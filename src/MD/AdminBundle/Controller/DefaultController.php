<?php

namespace MD\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MDAdminBundle:Default:index.html.twig');
    }
}
