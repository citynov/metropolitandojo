<?php

namespace MD\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActionHistory
 *
 * @ORM\Table(name="action_history")
 * @ORM\Entity(repositoryClass="MD\AdminBundle\Repository\ActionHistoryRepository")
 */
class ActionHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_action_history", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="object", type="string", length=45)
     */
    private $object;

    /**
     * @var int
     *
     * @ORM\Column(name="id_object", type="integer")
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(name="action_type", type="string", length=20)
     */
    private $actionType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="action_date", type="datetime")
     */
    private $actionDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set object
     *
     * @param string $object
     *
     * @return ActionHistory
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set idObject
     *
     * @param integer $idObject
     *
     * @return ActionHistory
     */
    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;

        return $this;
    }

    /**
     * Get idObject
     *
     * @return int
     */
    public function getIdObject()
    {
        return $this->idObject;
    }

    /**
     * Set actionType
     *
     * @param string $actionType
     *
     * @return ActionHistory
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;

        return $this;
    }

    /**
     * Get actionType
     *
     * @return string
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * Set actionDate
     *
     * @param \DateTime $actionDate
     *
     * @return ActionHistory
     */
    public function setActionDate($actionDate)
    {
        $this->actionDate = $actionDate;

        return $this;
    }

    /**
     * Get actionDate
     *
     * @return \DateTime
     */
    public function getActionDate()
    {
        return $this->actionDate;
    }

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param int $objectId
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
    }
}
