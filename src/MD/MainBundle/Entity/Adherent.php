<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use MD\UserBundle\Entity\Teacher;

/**
 * Adherent
 *
 * @ORM\Table(name="adherent")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\AdherentRepository")
 */
class Adherent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Activity
     *
     * @ManyToOne(targetEntity="Activity")
     * @JoinColumn(name="id_activity", referencedColumnName="id_activity")
     */
    private $activity;

    /**
     * @var Teacher
     *
     * @ManyToOne(targetEntity="Teacher")
     * @JoinColumn(name="id_teacher", referencedColumnName="id_teacher")
     */
    private $teacher;

    /**
     * Todo : Arrêter ici pour les assoc
     * @var int
     *
     * @ORM\Column(name="id_lead", type="integer")
     */
    private $idLead;

    /**
     * @var int
     *
     * @ORM\Column(name="id_group_adherent", type="integer")
     */
    private $idGroupAdherent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_address", type="integer")
     */
    private $idAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=40)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=20)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15)
     */
    private $phone;

    /**
     * @var int
     *
     * @ORM\Column(name="sexe", type="integer")
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="text")
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="string", length=20)
     */
    private $experience;

    /**
     * @var string
     *
     * @ORM\Column(name="motivations", type="text")
     */
    private $motivations;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="date")
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_type", type="string", length=50)
     */
    private $paymentType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_add", type="datetime")
     */
    private $dateAdd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime")
     */
    private $dateUpdate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activity
     *
     * @param Activity $activity
     *
     * @return Adherent
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get idActivity
     *
     * @return Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set teacher
     *
     * @param Teacher $teacher
     *
     * @return Adherent
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * Get teacher
     *
     * @return Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Set idLead
     *
     * @param integer $idLead
     *
     * @return Adherent
     */
    public function setIdLead($idLead)
    {
        $this->idLead = $idLead;

        return $this;
    }

    /**
     * Get idLead
     *
     * @return int
     */
    public function getIdLead()
    {
        return $this->idLead;
    }

    /**
     * Set idGroupAdherent
     *
     * @param integer $idGroupAdherent
     *
     * @return Adherent
     */
    public function setIdGroupAdherent($idGroupAdherent)
    {
        $this->idGroupAdherent = $idGroupAdherent;

        return $this;
    }

    /**
     * Get idGroupAdherent
     *
     * @return int
     */
    public function getIdGroupAdherent()
    {
        return $this->idGroupAdherent;
    }

    /**
     * Set idAddress
     *
     * @param integer $idAddress
     *
     * @return Adherent
     */
    public function setIdAddress($idAddress)
    {
        $this->idAddress = $idAddress;

        return $this;
    }

    /**
     * Get idAddress
     *
     * @return int
     */
    public function getIdAddress()
    {
        return $this->idAddress;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Adherent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Adherent
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Adherent
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Adherent
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set sexe
     *
     * @param integer $sexe
     *
     * @return Adherent
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return int
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Adherent
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set experience
     *
     * @param string $experience
     *
     * @return Adherent
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return string
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set motivations
     *
     * @param string $motivations
     *
     * @return Adherent
     */
    public function setMotivations($motivations)
    {
        $this->motivations = $motivations;

        return $this;
    }

    /**
     * Get motivations
     *
     * @return string
     */
    public function getMotivations()
    {
        return $this->motivations;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Adherent
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set paymentType
     *
     * @param string $paymentType
     *
     * @return Adherent
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     *
     * @return Adherent
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Adherent
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
}

