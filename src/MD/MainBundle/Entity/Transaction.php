<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_transaction", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_order", type="integer")
     */
    private $idOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=20)
     */
    private $transactionId;

    /**
     * @var string
     *
     * @ORM\Column(name="card_number", type="string", length=16)
     */
    private $cardNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="card_brant", type="integer")
     */
    private $cardBrant;

    /**
     * @var string
     *
     * @ORM\Column(name="card_expiration", type="string", length=7)
     */
    private $cardExpiration;

    /**
     * @var string
     *
     * @ORM\Column(name="card_holder", type="string", length=4)
     */
    private $cardHolder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_transaction", type="datetime")
     */
    private $dateTransaction;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idOrder
     *
     * @param integer $idOrder
     *
     * @return Transaction
     */
    public function setIdOrder($idOrder)
    {
        $this->idOrder = $idOrder;

        return $this;
    }

    /**
     * Get idOrder
     *
     * @return int
     */
    public function getIdOrder()
    {
        return $this->idOrder;
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     *
     * @return Transaction
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set cardNumber
     *
     * @param string $cardNumber
     *
     * @return Transaction
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get cardNumber
     *
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set cardBrant
     *
     * @param integer $cardBrant
     *
     * @return Transaction
     */
    public function setCardBrant($cardBrant)
    {
        $this->cardBrant = $cardBrant;

        return $this;
    }

    /**
     * Get cardBrant
     *
     * @return int
     */
    public function getCardBrant()
    {
        return $this->cardBrant;
    }

    /**
     * Set cardExpiration
     *
     * @param string $cardExpiration
     *
     * @return Transaction
     */
    public function setCardExpiration($cardExpiration)
    {
        $this->cardExpiration = $cardExpiration;

        return $this;
    }

    /**
     * Get cardExpiration
     *
     * @return string
     */
    public function getCardExpiration()
    {
        return $this->cardExpiration;
    }

    /**
     * Set cardHolder
     *
     * @param string $cardHolder
     *
     * @return Transaction
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;

        return $this;
    }

    /**
     * Get cardHolder
     *
     * @return string
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * Set dateTransaction
     *
     * @param \DateTime $dateTransaction
     *
     * @return Transaction
     */
    public function setDateTransaction($dateTransaction)
    {
        $this->dateTransaction = $dateTransaction;

        return $this;
    }

    /**
     * Get dateTransaction
     *
     * @return \DateTime
     */
    public function getDateTransaction()
    {
        return $this->dateTransaction;
    }
}

