<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_page", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idPage;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="url_rewrite", type="string", length=50)
     */
    private $urlRewrite;


    /**
     * Get idPage
     *
     * @return int
     */
    public function getId()
    {
        return $this->idPage;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Page
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set urlRewrite
     *
     * @param string $urlRewrite
     *
     * @return Page
     */
    public function setUrlRewrite($urlRewrite)
    {
        $this->urlRewrite = $urlRewrite;

        return $this;
    }

    /**
     * Get urlRewrite
     *
     * @return string
     */
    public function getUrlRewrite()
    {
        return $this->urlRewrite;
    }
}

