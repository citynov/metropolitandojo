<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Custom_FIeld
 *
 * @ORM\Table(name="custom_field")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\CustomFieldRepository")
 */
class CustomField
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_custom_field", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="moral", type="text")
     */
    private $moral;

    /**
     * @var int
     *
     * @ORM\Column(name="id_teacher", type="integer")
     */
    private $idTeacher;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Custom_FIeld
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set moral
     *
     * @param string $moral
     *
     * @return Custom_FIeld
     */
    public function setMoral($moral)
    {
        $this->moral = $moral;

        return $this;
    }

    /**
     * Get moral
     *
     * @return string
     */
    public function getMoral()
    {
        return $this->moral;
    }

    /**
     * Set idTeacher
     *
     * @param integer $idTeacher
     *
     * @return Custom_FIeld
     */
    public function setIdTeacher($idTeacher)
    {
        $this->idTeacher = $idTeacher;

        return $this;
    }

    /**
     * Get idTeacher
     *
     * @return int
     */
    public function getIdTeacher()
    {
        return $this->idTeacher;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Custom_FIeld
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

