<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Affiliate
 *
 * @ORM\Table(name="affiliate")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\AffiliateRepository")
 */
class Affiliate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="affiliate_host", type="string", length=45)
     */
    private $affiliateHost;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_visitor_affiliate", type="integer")
     */
    private $nbVisitorAffiliate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set affiliateHost
     *
     * @param string $affiliateHost
     *
     * @return Affiliate
     */
    public function setAffiliateHost($affiliateHost)
    {
        $this->affiliateHost = $affiliateHost;

        return $this;
    }

    /**
     * Get affiliateHost
     *
     * @return string
     */
    public function getAffiliateHost()
    {
        return $this->affiliateHost;
    }

    /**
     * Set nbVisitorAffiliate
     *
     * @param integer $nbVisitorAffiliate
     *
     * @return Affiliate
     */
    public function setNbVisitorAffiliate($nbVisitorAffiliate)
    {
        $this->nbVisitorAffiliate = $nbVisitorAffiliate;

        return $this;
    }

    /**
     * Get nbVisitorAffiliate
     *
     * @return int
     */
    public function getNbVisitorAffiliate()
    {
        return $this->nbVisitorAffiliate;
    }
}

