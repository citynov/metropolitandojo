<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Booking
 *
 * @ORM\Table(name="booking")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\BookingRepository")
 */
class Booking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MainBundle\Entity\Teacher")
     * 
     * @ORM\Column(name="id_teacher", type="integer")
     */
    private $idTeacher;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MainBundle\Entity\Room")
     *
     * @ORM\Column(name="id_room", type="integer")
     */
    private $idRoom;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MainBundle\Entity\Activity")
     *
     * @ORM\Column(name="id_activity", type="integer")
     */
    private $idActivity;

    /**
     * @ORM\ManyToOne(targetEntity="MD\MainBundle\Entity\Internship")
     * 
     * @ORM\Column(name="id_internship", type="integer")
     */
    private $idInternship;

    /**
     * @var int
     *
     * @ORM\Column(name="recursivity", type="integer")
     */
    private $recursivity;

    /**
     * @var int
     *
     * @ORM\Column(name="day", type="integer")
     */
    private $day;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="date")
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="date")
     */
    private $dateEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_start", type="time")
     */
    private $timeStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_stop", type="time")
     */
    private $timeStop;

    /**
     * @var bool
     *
     * @ORM\Column(name="validate", type="boolean")
     */
    private $validate = 0;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTeacher
     *
     * @param integer $idTeacher
     *
     * @return Booking
     */
    public function setIdTeacher($idTeacher)
    {
        $this->idTeacher = $idTeacher;

        return $this;
    }

    /**
     * Get idTeacher
     *
     * @return integer
     */
    public function getIdTeacher()
    {
        return $this->idTeacher;
    }

    /**
     * Set idRoom
     *
     * @param integer $idRoom
     *
     * @return Booking
     */
    public function setIdRoom($idRoom)
    {
        $this->idRoom = $idRoom;

        return $this;
    }

    /**
     * Get idRoom
     *
     * @return integer
     */
    public function getIdRoom()
    {
        return $this->idRoom;
    }

    /**
     * Set idActivity
     *
     * @param integer $idActivity
     *
     * @return Booking
     */
    public function setIdActivity($idActivity)
    {
        $this->idActivity = $idActivity;

        return $this;
    }

    /**
     * Get idActivity
     *
     * @return integer
     */
    public function getIdActivity()
    {
        return $this->idActivity;
    }

    /**
     * Set idInternship
     *
     * @param integer $idInternship
     *
     * @return Booking
     */
    public function setIdInternship($idInternship)
    {
        $this->idInternship = $idInternship;

        return $this;
    }

    /**
     * Get idInternship
     *
     * @return integer
     */
    public function getIdInternship()
    {
        return $this->idInternship;
    }

    /**
     * Set recursivity
     *
     * @param integer $recursivity
     *
     * @return Booking
     */
    public function setRecursivity($recursivity)
    {
        $this->recursivity = $recursivity;

        return $this;
    }

    /**
     * Get recursivity
     *
     * @return integer
     */
    public function getRecursivity()
    {
        return $this->recursivity;
    }

    /**
     * Set day
     *
     * @param integer $day
     *
     * @return Booking
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return integer
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Booking
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Booking
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     *
     * @return Booking
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeStop
     *
     * @param \DateTime $timeStop
     *
     * @return Booking
     */
    public function setTimeStop($timeStop)
    {
        $this->timeStop = $timeStop;

        return $this;
    }

    /**
     * Get timeStop
     *
     * @return \DateTime
     */
    public function getTimeStop()
    {
        return $this->timeStop;
    }

    /**
     * Set validate
     *
     * @param boolean $validate
     *
     * @return Booking
     */
    public function setValidate($validate)
    {
        $this->validate = $validate;

        return $this;
    }

    /**
     * Get validate
     *
     * @return boolean
     */
    public function getValidate()
    {
        return $this->validate;
    }
}
