<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sale
 *
 * @ORM\Table(name="order")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\SaleRepository")
 */
class Sale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_order", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idSale;

    /**
     * @var string
     *
     * @ORM\Column(name="price_ht", type="decimal", precision=10, scale=2)
     */
    private $priceHt;

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="decimal", precision=10, scale=2)
     */
    private $discount;

    /**
     * @var int
     *
     * @ORM\Column(name="frequency", type="integer")
     */
    private $frequency;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_nb", type="integer")
     */
    private $paymentNb;

    /**
     * @var int
     *
     * @ORM\Column(name="order_statut", type="integer")
     */
    private $saleStatut;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->idSale;
    }

    /**
     * Set priceHt
     *
     * @param string $priceHt
     *
     * @return Sale
     */
    public function setPriceHt($priceHt)
    {
        $this->priceHt = $priceHt;

        return $this;
    }

    /**
     * Get priceHt
     *
     * @return string
     */
    public function getPriceHt()
    {
        return $this->priceHt;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return Sale
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set frequency
     *
     * @param integer $frequency
     *
     * @return Sale
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return int
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set paymentNb
     *
     * @param integer $paymentNb
     *
     * @return Sale
     */
    public function setPaymentNb($paymentNb)
    {
        $this->paymentNb = $paymentNb;

        return $this;
    }

    /**
     * Get paymentNb
     *
     * @return int
     */
    public function getPaymentNb()
    {
        return $this->paymentNb;
    }

    /**
     * Set saleStatut
     *
     * @param integer $saleStatut
     *
     * @return Sale
     */
    public function setSaleStatut($saleStatut)
    {
        $this->saleStatut = $saleStatut;

        return $this;
    }

    /**
     * Get saleStatut
     *
     * @return int
     */
    public function getSaleStatut()
    {
        return $this->saleStatut;
    }
}

