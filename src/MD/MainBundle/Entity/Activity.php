<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\ActivityRepository")
 */
class Activity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Image
     *
     * @ManyToOne(targetEntity="Image")
     * @JoinColumn(name="id_main_image", referencedColumnName="id_image")
     */
    private $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="accessibility", type="text")
     */
    private $accessibility;

    /**
     * @var string
     *
     * @ORM\Column(name="redundancy", type="string", length=255)
     */
    private $redundancy;

    /**
     * @var string
     *
     * @ORM\Column(name="prerequisites", type="text")
     */
    private $prerequisites;

    /**
     * @var string
     *
     * @ORM\Column(name="equipment", type="text", nullable=true)
     */
    private $equipment;

    /**
     * @var bool
     *
     * @ORM\Column(name="first_session", type="boolean")
     */
    private $firstSession;

    /**
     * @var string
     *
     * @ORM\Column(name="url_video", type="string", length=100, nullable=true)
     */
    private $urlVideo;

    /**
     * @var int
     *
     * @ORM\Column(name="category", type="integer")
     */
    private $category;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Activity
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Activity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mainImage
     *
     * @param Image $image
     *
     * @return Activity
     */
    public function setMainImage($image)
    {
        $this->mainImage = $image;

        return $this;
    }

    /**
     * Get Image
     *
     * @return Image
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * Set accessibility
     *
     * @param string $accessibility
     *
     * @return Activity
     */
    public function setAccessibility($accessibility)
    {
        $this->accessibility = $accessibility;

        return $this;
    }

    /**
     * Get accessibility
     *
     * @return string
     */
    public function getAccessibility()
    {
        return $this->accessibility;
    }

    /**
     * Set redundancy
     *
     * @param string $redundancy
     *
     * @return Activity
     */
    public function setRedundancy($redundancy)
    {
        $this->redundancy = $redundancy;

        return $this;
    }

    /**
     * Get redundancy
     *
     * @return string
     */
    public function getRedundancy()
    {
        return $this->redundancy;
    }

    /**
     * Set prerequisites
     *
     * @param string $prerequisites
     *
     * @return Activity
     */
    public function setPrerequisites($prerequisites)
    {
        $this->prerequisites = $prerequisites;

        return $this;
    }

    /**
     * Get prerequisites
     *
     * @return string
     */
    public function getPrerequisites()
    {
        return $this->prerequisites;
    }

    /**
     * Set equipment
     *
     * @param string $equipment
     *
     * @return Activity
     */
    public function setEquipment($equipment)
    {
        $this->equipment = $equipment;

        return $this;
    }

    /**
     * Get equipment
     *
     * @return string
     */
    public function getEquipment()
    {
        return $this->equipment;
    }

    /**
     * Set firstSession
     *
     * @param boolean $firstSession
     *
     * @return Activity
     */
    public function setFirstSession($firstSession)
    {
        $this->firstSession = $firstSession;

        return $this;
    }

    /**
     * Get firstSession
     *
     * @return bool
     */
    public function getFirstSession()
    {
        return $this->firstSession;
    }

    /**
     * Set urlVideo
     *
     * @param string $urlVideo
     *
     * @return Activity
     */
    public function setUrlVideo($urlVideo)
    {
        $this->urlVideo = $urlVideo;

        return $this;
    }

    /**
     * Get urlVideo
     *
     * @return string
     */
    public function getUrlVideo()
    {
        return $this->urlVideo;
    }

    /**
     * Set category
     *
     * @param integer $category
     *
     * @return Activity
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Activity
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }
}

