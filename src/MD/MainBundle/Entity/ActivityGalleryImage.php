<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityGalleryImage
 *
 * @ORM\Table(name="activity_gallery_image")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\ActivityGalleryImageRepository")
 */
class ActivityGalleryImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Activity
     *
     * @ManyToOne(targetEntity="Activity")
     * @JoinColumn(name="id_activity", referencedColumnName="id_activity")
     */
    private $activity;

    /**
     * @var Image
     *
     * @ManyToOne(targetEntity="Image")
     * @JoinColumn(name="id_image", referencedColumnName="id_image")
     */
    private $image;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activity
     *
     * @param Activity $idActivity
     *
     * @return ActivityGalleryImage
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set image
     *
     * @param Image $image
     *
     * @return ActivityGalleryImage
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Image
     */
    public function getImage()
    {
        return $this->image;
    }
}

