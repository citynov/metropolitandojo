<?php

namespace MD\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Internship
 *
 * @ORM\Table(name="internship")
 * @ORM\Entity(repositoryClass="MD\MainBundle\Repository\InternshipRepository")
 */
class Internship
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_internship", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="id_main_image", type="integer")
     */
    private $mainImage;

    /**
     * @var string
     *
     * @ORM\Column(name="accessibility", type="text")
     */
    private $accessibility;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=255)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="prerequisites", type="text")
     */
    private $prerequisites;

    /**
     * @var int
     * @todo replace by relation
     * @ORM\Column(name="id_teacher", type="integer")
     */
    private $teacher;

    /**
     * @var string
     *
     * @ORM\Column(name="public_target", type="text")
     */
    private $publicTarget;

    /**
     * @todo replace by relation
     * @var int
     *
     * @ORM\Column(name="id_pole", type="integer")
     */
    private $pole;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Internship
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Internship
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set accessibility
     *
     * @param string $accessibility
     *
     * @return Internship
     */
    public function setAccessibility($accessibility)
    {
        $this->accessibility = $accessibility;

        return $this;
    }

    /**
     * Get accessibility
     *
     * @return string
     */
    public function getAccessibility()
    {
        return $this->accessibility;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return Internship
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set prerequisites
     *
     * @param string $prerequisites
     *
     * @return Internship
     */
    public function setPrerequisites($prerequisites)
    {
        $this->prerequisites = $prerequisites;

        return $this;
    }

    /**
     * Get prerequisites
     *
     * @return string
     */
    public function getPrerequisites()
    {
        return $this->prerequisites;
    }

    /**
     * Set publicTarget
     *
     * @param string $publicTarget
     *
     * @return Internship
     */
    public function setPublicTarget($publicTarget)
    {
        $this->publicTarget = $publicTarget;

        return $this;
    }

    /**
     * Get publicTarget
     *
     * @return string
     */
    public function getPublicTarget()
    {
        return $this->publicTarget;
    }

    /**
     * Set idPole
     *
     * @param integer $idPole
     *
     * @return Internship
     */
    public function setIdPole($idPole)
    {
        $this->idPole = $idPole;

        return $this;
    }

    /**
     * Get idPole
     *
     * @return int
     */
    public function getIdPole()
    {
        return $this->idPole;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Internship
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Internship
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return int
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param int $teacher
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @return int
     */
    public function getMainImage()
    {
        return $this->mainImage;
    }

    /**
     * @param int $mainImage
     */
    public function setMainImage($mainImage)
    {
        $this->mainImage = $mainImage;
    }

    /**
     * Set pole
     *
     * @param integer $pole
     *
     * @return Internship
     */
    public function setPole($pole)
    {
        $this->pole = $pole;

        return $this;
    }

    /**
     * Get pole
     *
     * @return integer
     */
    public function getPole()
    {
        return $this->pole;
    }
}
